<?php
require_once(__DIR__ . '/vendor/autoload.php');

use App\Observer;
use App\Observable;

$observable = new Observable();
$observable->attach(new Observer());

$i = 0;
while($i < 10) {
    $observable->setData($i);
    $observable->notify();
    $i++;
}
