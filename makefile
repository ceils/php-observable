init: dl composer.install server.start

server.start:
	php -S 127.0.0.1:8000

composer.install:
	php composer.phar install -o

dl:
	-rm phploc.phar
	-rm composer.phar
	-rm phpmd.phar
	-rm php-cs-fixer.phar
	wget https://phar.phpunit.de/phploc.phar
	wget https://getcomposer.org/composer.phar
	wget http://static.phpmd.org/php/latest/phpmd.phar
	wget http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -O php-cs-fixer.phar

phpcsfixer:
	php php-cs-fixer.phar fix src/

phploc:
	php phploc.phar src/

phpmd:
	php phpmd.phar src/ html phpmd/phpmd.xml > phpmd/phpmd.html