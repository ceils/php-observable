<?php
namespace App;

use App\Pattern\ObserverInterface;
use App\Pattern\ObservableInterface;

/**
 * Class Observable
 *
 * @package App
 */
class Observable implements ObservableInterface
{
    /**
     * @var \SplObjectStorage
     */
    protected $observers;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * Observable constructor.
     */
    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    /**
     * @inheritdoc
     */
    public function attach(ObserverInterface $observer)
    {
        $this->observers->attach($observer);
    }

    /**
     * @inheritdoc
     */
    public function detach(ObserverInterface $observer)
    {
        $this->observers->detach($observer);
    }

    /**
     * @inheritdoc
     */
    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data= $data;
    }
}
