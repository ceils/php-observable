<?php
namespace App;

use App\Pattern\ObserverInterface;
use App\Pattern\ObservableInterface;

/**
 * Class Observer
 *
 * @package App
 */
class Observer implements ObserverInterface
{

    /**
     * @inheritdoc
     */
    public function update(ObservableInterface $observable)
    {
        echo '<pre>';
        print_r(__CLASS__ . ' : ' . $observable->getData());
        echo '</pre>';
    }
}
