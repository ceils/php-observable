<?php
namespace App\Pattern;

/**
 * Interface ObservableInterface
 *
 * @package App\Pattern
 */
interface ObservableInterface
{
    /**
     * Attach an observer
     *
     * @param ObserverInterface $observer
     */
    public function attach(ObserverInterface $observer);

    /**
     * Detach an observer
     *
     * @param ObserverInterface $observer
     */
    public function detach(ObserverInterface $observer);

    /**
     * Notify an observer
     */
    public function notify();
}
