<?php
namespace App\Pattern;

/**
 * Interface ObserverInterface
 *
 * @package App\Pattern
 */
interface ObserverInterface
{

    /**
     * @param ObservableInterface $observable
     */
    public function update(ObservableInterface $observable);
}
